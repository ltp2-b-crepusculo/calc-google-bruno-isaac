package operators

import (
	"errors"
	"math"
	"strconv"
	"strings"
)

type Pot struct {
	expression string
	Resultado  float64
}

func (p Pot) Calculate(expression string) (float64, error) {
	var operands []string
	switch {
	case strings.Contains(expression, "^"):
		operands = strings.Split(expression, "^")
	case strings.Contains(expression, `pow`):
		operands = strings.Split(expression, "pow")
	}

	// Converter os operandos para números
	base, err := strconv.ParseFloat(strings.TrimSpace(operands[0]), 64)
	if err != nil {
		return 0, errors.New("base is not a valid number")
	}

	exponent, err := strconv.ParseFloat(strings.TrimSpace(operands[1]), 64)
	if err != nil {
		return 0, errors.New("exponent is not a valid number")
	}

	// Realizar a potência
	result := math.Pow(base, exponent)

	return result, nil
}
