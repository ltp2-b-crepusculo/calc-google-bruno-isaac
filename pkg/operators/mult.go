package operators

import (
	"errors"
	"strconv"
	"strings"
)

type Mult struct {
	expression string
	Resultado  float64
}

func (m Mult) Calculate(expression string) (float64, error) {
	var operands []string
	switch {
	case strings.Contains(expression, "*"):
		operands = strings.Split(expression, "*")
	case strings.Contains(expression, `mul`):
		operands = strings.Split(expression, "mul")
	}
	// Converter os operandos para números
	operand1, err := strconv.ParseFloat(strings.TrimSpace(operands[0]), 64)
	if err != nil {
		return 0, errors.New("first operand is not a valid number")
	}

	operand2, err := strconv.ParseFloat(strings.TrimSpace(operands[1]), 64)
	if err != nil {
		return 0, errors.New("second operand is not a valid number")
	}

	result := operand1 * operand2

	return result, nil
}
