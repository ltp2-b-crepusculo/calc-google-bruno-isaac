package operators

import (
	"errors"
	"strconv"
	"strings"
)

type Div struct {
	expression string
	Resultado  float64
}

func (d Div) Calculate(expression string) (float64, error) {
	// Dividir a expressão nos operandos

	var operands []string
	switch {
	case strings.Contains(expression, "/"):
		operands = strings.Split(expression, "/")
	case strings.Contains(expression, `div`):
		operands = strings.Split(expression, "div")
	}
	//if len(parts) != 2 {
	//return 0, errors.New("invalid expression format")
	//}

	// Converter os operandos para números
	dividend, err := strconv.ParseFloat(strings.TrimSpace(operands[0]), 64)
	if err != nil {
		return 0, errors.New("dividend is not a valid number")
	}

	divisor, err := strconv.ParseFloat(strings.TrimSpace(operands[1]), 64)
	if err != nil {
		return 0, errors.New("divisor is not a valid number")
	}

	// Verificar se o divisor é zero
	if divisor == 0 {
		return 0, errors.New("division by zero")
	}
	// Realizar a divisão
	result := dividend / divisor

	return result, nil
}
