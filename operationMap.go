package main

import (
	"errors"
	"localhost.com/go/pkg/operators"
	"strconv"
	"strings"
)

type Calcular interface {
	Calculate(expression string) (float64, error)
}

// Mapa de operações
var operacoesMap = map[string]Calcular{
	"sum": operators.Soma{},
	"+":   operators.Soma{},
	"mul": operators.Mult{},
	"*":   operators.Mult{},
	"sub": operators.Sub{},
	"-":   operators.Sub{},
	"div": operators.Div{},
	"/":   operators.Div{},
	"pow": operators.Pot{},
	"^":   operators.Pot{},
	"rot": operators.Raiz{},
	"&":   operators.Raiz{},
}

func validateValues(op string, expression string) error {
	switch op {
	case "sum", "sub", "mul", "div", "pow", "rot", "+", "*", "-", "/", "^", "&":
		// Para operações que esperam dois operandos, verifique se há dois números na expressão
		parts := strings.Split(expression, op)
		if len(parts) != 2 {
			return errors.New("Não foram encontrados numeros operadores separados por " + op)
		}
		// Verifique se ambos os operandos são números válidos
		_, err := strconv.ParseFloat(strings.TrimSpace(parts[0]), 64)
		if err != nil {
			return errors.New("Primeiro operador não é um numero valido")
		}
		_, err = strconv.ParseFloat(strings.TrimSpace(parts[1]), 64)
		if err != nil {
			return errors.New("Segundo operador não é um numero valido")
		}
	default:
		return errors.New("Operação invalida")
	}
	return nil
}
